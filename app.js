const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
// const expressValidator = require('express-validator');
const { check, validationResult } = require('express-validator'); 
require('dotenv').config();
// import routes
const authRoutes = require('./routes/auth');
const userRoutes = require('./routes/user');
const categoryRoutes = require('./routes/category');
const companyRoutes = require('./routes/company');
const productRoutes = require('./routes/product');
const paymentRoutes = require('./routes/product');
// const braintreeRoutes = require('./routes/braintree');
const orderRoutes = require('./routes/order');

// app
const app = express();

// db
mongoose
    .connect(process.env.DATABASE, {
        // useNewUrlParser: true,
        // useCreateIndex: true
    })
    .then(() => console.log('DB Connected'));

// middlewares
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(check());
app.use(cors());

// routes middleware
app.use('/api', authRoutes);
app.use('/api', userRoutes);
app.use('/api', categoryRoutes);
app.use('/api', productRoutes);
// app.use('/api', braintreeRoutes);
app.use('/api', companyRoutes);
app.use('/api', orderRoutes);
app.use('/api', paymentRoutes);
const port = process.env.PORT || 8000;

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
