
const { errorHandler } = require('../helpers/dbErrorHandler');
const Payment = require('../models/payment');
const { JunoCardHash } = require('juno-nodejs');
exports.paymentById = (req, res, next, id) => {
    Payment.findById(id).exec((err, payment) => {
        if (err || !payment) {
            return res.status(400).json({
                error: 'payment does not exist'
            });
        }
        req.payment = payment;
        next();
    });
};

exports.create= async (req, res) => {
   

 

    
  

};
exports. payWithPixBill=async  (order) => {
    var token = await this.createToken()
    var billing = await this.generateBilling(token, order.amount, company,  order.user, order.address) 
    var charge = await this.generatePayment(token, order.user, hashcard , billing.chargeId, order.address)
    const payment = new Payment(req.body.order);
        payment.save((error, data) => {
            if (error) {
                return res.status(400).json({
                    error: errorHandler(error)
                });
            }
 
            res.json(data);
        });

};
exports.payWithCC= async (req, res) => {
   const  {
        holderName,
        cardNumber,
        securityCode,
        expirationMonth,
        expirationYear,
    } =req.body.creditCard
    const credidcardData = {
        holderName,
        cardNumber,
        securityCode,
        expirationMonth,
        expirationYear,
    }
    const order= req.body.order
    try{
        var token = await this.createToken()
        var hashcard = await this.createCreditCarToken(credidcardData)
        var billing = await this.generateBilling(token, order.amount, company,  order.user, order.address) 
        var charge = await this.generatePayment(token, order.user, hashcard , billing.chargeId, order.address)
        var paymentJuno = this.capturePayment(token, order.amount, billing.chargeId) 
        const payment = new Payment({});
        payment.save((error, data) => {
            if (error) {
                return res.status(400).json({
                    error: errorHandler(error)
                });
            }
 
            res.json(data);
        });
    }catch(e){
        return null
    }


};

exports.createCreditCarToken = async (creditCard) => {
    const {
        holderName,
        cardNumber,
        securityCode,
        expirationMonth,
        expirationYear,
    }=creditCard
    const publicToken = ''; // Token público da api da JUNO
    const environment = 'sandbox'; // 'sandbox' || 'production'
    const cardData = {
        holderName: holderName,
        cardNumber: cardNumber,
        securityCode: securityCode,
        expirationMonth:  expirationMonth,
        expirationYear: expirationYear,
    };

    const junoService = new JunoCardHash(publicToken, environment);

    var hash = await junoService.getCardHash(cardData)
       
    return hash

};

exports.generateBilling = async(token, amount, company,  user, address) => {
    const config = {
      headers: {
        'X-Api-Version': '2',
        'X-Resource-Token':  '3D823DE3515F7CABEF745F35F0161D10FF956E866A79686E24FABE50BB07B209', 
        'Authorization': `Bearer ${token}`,
      }
    }
    var dueDate = new Date().getDate();
  
    var payload = {
      charge: {
        pixKey: "ad05fbdd-2aa8-479b-bef3-a05b6171f429", // de onde vem essa key ?
        "pixIncludeImage": true,
        description: "Compra ",
        references: [],
        amount: amount,
        dueDate: dueDate,
        installments: 1,
        maxOverdueDays: 0,
        fine: 0,
        interest: "0.00",
        paymentTypes: [
          "CREDIT_CARD",
          "BOLETO_PIX"
        ],
        paymentAdvance: true,
        split: [
          { //JUNO_PAY_KEY
            "recipientToken":  '3D823DE3515F7CABEF745F35F0161D10FF956E866A79686E24FABE50BB07B209', 
            "percentage": 10,
            "amountRemainder": true,
            "chargeFee": true,
          },
          {
            "recipientToken": company.juno_token,
            "percentage": 90,
            "amountRemainder": false,
            "chargeFee": false
          }
        ]
      },
      billing: {
        name: user.name,
        document: user.document,
        email: user.email,
        complement: address.complement,
          neighborhood: address.neighborhood,
          city: address.city,
          state: address.state,
          postCode: address.postCode
        },
        phone: user.cell,
        birthDate: user.birth_date,
        notify: false
      }
    

    try {
      var response = await axios.post(`https://sandbox.boletobancario.com/api-integration/charges`, payload, config)
      console.log(response.data._embedded)
      if (response.data._embedded) {
        return response.data._embedded.charges
      }
      else {
        throw error
      }
    } catch (error) {
      console.log(error.response.data);
      return error
    }

  }

  exports.generatePayment=(token, user, hash_card, chargeId, address) =>{
    const config = {
      headers: {
        'X-Api-Version': '2',
        'X-Resource-Token': '3D823DE3515F7CABEF745F35F0161D10FF956E866A79686E24FABE50BB07B209',
        'Authorization': `Bearer ${token}`,
      }
    }
    const body = {
      chargeId: chargeId,
      billing: {
        email: user.email,
        address: {
          street: address.street,
          number: address.number,
          complement: address.complement,
          neighborhood: address.neighborhood,
          city: address.city,
          state: address.state,
          postCode: address.postCode
        },
        delayed: false
      },
      creditCardDetails: {
        // creditCardId: "string",
        creditCardHash: hash_card
      }
    }
    try {

      return await axios.post(`https://sandbox.boletobancario.com/api-integration/payments/`, body, config)
    } catch (error) {
      // handle error
      console.log(error.response.data);
    }
  }

  exports.capturePayment= async (token, amount, chargeId) =>{
    const config = {
      Authentication: `Bearer ${token}`
    }
    const body = {
      chargeId: chargeId,
      amount: amount
    }
    const url = Env.get("APP_NAME", "AdonisJs"); // dúvida sobre esse Env e id no param do axios
    var response = await axios.post(`${url}/api-integration/payments/${id}/capture`, body, config)
    return response;
  }

exports.read = (req, res) => {
    return res.json(req.company);
};
//webhook
exports.update = async (req, res) => {
    console.log('req.body', req.body);

    const {junoId}=req.body
   await Payment.findOneAndUpdate(
    {
        junoId:junoId
    },
    {
        status:'PAID'
    });
};

exports.remove = (req, res) => {
    const company = req.company;

    company.remove((err, data) => {
        if (err) {
            return res.status(400).json({
                error: errorHandler(err)
            });
        }
        res.json({
            message: 'Company deleted'
        });
    });

};

exports.list = (req, res) => {
    Company.find().exec((err, data) => {
        if (err) {
            return res.status(400).json({
                error: errorHandler(err)
            });
        }
        res.json(data);
    });
};
