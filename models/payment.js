const mongoose = require("mongoose");

const paymentSchema = new mongoose.Schema(
    {
        user: {
            type: String,
            trim: true,
            required: true,
            maxlength: 32
        },
        status: {
            type: String,
            trim: true,
            required: true,
            maxlength: 32
        },
        value: {
            type: String,
            trim: true,
            required: true,
            maxlength: 32
        },
        order: {
            type: String,
            trim: true,
            required: true,
            maxlength: 32
        },
        vendor: {
            type: String,
            trim: true,
            required: true,
            maxlength: 32
        },
        paymentMethod: {
            type: String,
            trim: true,
            required: true,
            maxlength: 32
        },

    },
    { timestamps: true }
);

module.exports = mongoose.model("Payment", paymentSchema);
