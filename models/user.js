const mongoose = require('mongoose');
const crypto = require('crypto');
const { ObjectId } = mongoose.Schema;

const { v4: uuidv4 } = require('uuid');

// const roleEnum  = {
//     BUYER : 0,
//     SELLER : 1
// } 

const userSchema = new mongoose.Schema(
    {
        companyId: { type: ObjectId, ref: "Company" },
        name: {
            type: String,
            trim: true,
            required: true,
            maxlength: 32
        },
        email: {
            type: String,
            trim: true,
            required: true,
            unique: true
        },
        hashed_password: {
            type: String,
            required: true
        },
        about: {
            type: String,
            trim: true
        },
        salt: String,
        role: {
            type: Number,
            default: 0,
            required: true
        },
        history: {
            type: Array,
            default: []
        }
    },
    { timestamps: true }
);

// virtual field
userSchema
    .virtual('password')
    .set(function(password) {
        this._password = password;
        this.salt =uuidv4();
        this.hashed_password = this.encryptPassword(password);
    })
    .get(function() {
        return this._password;
    });

userSchema.methods = {
    authenticate: function(plainText) {
        console.log('crypt',this.encryptPassword(plainText));
        return this.encryptPassword(plainText) === this.hashed_password;
    },

    encryptPassword: function(password) {
        if (!password) return '1';
        try {
            return crypto
                .createHash('sha1')
                .update(password)
                .digest('hex');
        } catch (err) {
            return '2';
        }
    }
};

module.exports = mongoose.model('User', userSchema);
