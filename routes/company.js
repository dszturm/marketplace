const express = require('express');
const router = express.Router();

const { create, categoryById, read, update, remove, list } = require('../controllers/company');
const { requireSignin, isAuth, isAdmin } = require('../controllers/auth');
const { userById } = require('../controllers/user');

//router.get('/company/:companyId', read);
router.post('/company/create', create);
// router.post('/category/create/:userId', requireSignin, isAuth, isAdmin, create);
// router.put('/category/:categoryUpdateId/:userId', requireSignin, isAuth, isAdmin, update);
//router.put('/company/:categoryId/:userId', requireSignin, isAuth, isAdmin, update);

//router.delete('/company/:companyId', requireSignin, isAuth, isAdmin, remove);
router.get('/company', list);

// router.param('categoryId', categoryById);
// router.param('userId', userById);


module.exports = router;
