const express = require("express");
const router = express.Router();
const {

    update,
    payWithCC,
    payWithPixBill
    
} = require("../controllers/order");
//router.post("/product/create/:userId", requireSignin, isAuth, isAdmin, create);
router.post("/payment/creditCard", requireSignin, isAuth, payWithCC);
router.post("/payment/pixbill",    payWithPixBill);
router.post("/payment/webhook",  update );
